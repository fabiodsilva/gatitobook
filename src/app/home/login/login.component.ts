import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AutenticacaoService } from 'src/app/autenticacao/autenticacao.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
usuario='';
senha='';
  constructor(private authService:AutenticacaoService,private router:Router) { }

  ngOnInit(): void {
  }

  login(){

    this.authService.autenticar(this.usuario,this.senha).subscribe({
      //next: (v) => console.log(v),
      complete: () => this.router.navigate(['animais']),
      error: (e) => {
        alert('Usuário ou senha inválido');
      console.error(e)
    }

  })


    /*this.authService.autenticar(this.usuario,this.senha).subscribe(()=>{
      console.log('Autenticado com sucesso!');

    },(error)=>{
      alert('Usuário ou senha inválido');
      console.log(error);
    })*/
  }

}
